package controller

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/dash128/Twitter_BackEnd/src/app/models"
	"bitbucket.org/dash128/Twitter_BackEnd/src/app/repository"
)

/*UsuarioController -> controlador para el usuario*/
type UsuarioController struct {
	repository repository.UsuarioRepository
}

/*Listar -> controlador que lista usuarios*/
func (controller *UsuarioController) Listar(response http.ResponseWriter, request *http.Request) {
	lista := controller.repository.Listar()

	json.NewEncoder(response).Encode(lista)
}

func (controller *UsuarioController) Registro(w http.ResponseWriter, r *http.Request) {
	var usuario models.Usuario
	err := json.NewDecoder(r.Body).Decode(&usuario)

	if err != nil {
		http.Error(w, "Error en los datos recibidos "+err.Error(), 400)
		return
	}

	if len(usuario.Email) == 0 {
		http.Error(w, "El email de usuario es requerido", 400)
	}

	if len(usuario.Password) < 6 {
		http.Error(w, "Debe especificar una contraseña de al menos 6 caracteres", 400)
		return
	}

	_, encontrado, _ := controller.repository.ExisteUsuario(usuario.Email) //bd.ExisteUsuario(usuario.Email)
	if encontrado == true {
		http.Error(w, "Ya existe un usuario registrado con el email", 400)
		return
	}

	_, status, _ := controller.repository.InsertarRegistro(usuario) //bd.InsertarRegistro(usuario)
	if err != nil {
		http.Error(w, "Ocurrio un error al intentar realizar el registro de usuario"+err.Error(), 400)
		return
	}

	if status == false {
		http.Error(w, "No se ha logrado insertar el registrp del usuario", 400)
		return
	}

	w.WriteHeader(http.StatusCreated)
}
