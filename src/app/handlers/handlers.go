package handlers

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/dash128/Twitter_BackEnd/src/app/controller"
	"bitbucket.org/dash128/Twitter_BackEnd/src/config/middlew"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

/*Manejadores -> Contiene las rutas existentes en el API*/
func Manejadores() {
	router := mux.NewRouter()

	usuarioController := controller.UsuarioController{}

	//router.HandleFunc("/registro", middlew.ChequeoBD(routers.Registro)).Methods("POST")
	router.HandleFunc("/registro", middlew.ChequeoBD(usuarioController.Registro)).Methods("POST")
	router.HandleFunc("/lista", middlew.ChequeoBD(usuarioController.Listar)).Methods("GET")
	PORT := os.Getenv("PORT")

	if PORT == "" {
		PORT = "8080"
	}

	handler := cors.AllowAll().Handler(router)

	log.Fatal(http.ListenAndServe(":"+PORT, handler))
}
