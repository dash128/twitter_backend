package repository

import (
	"context"
	"time"

	"bitbucket.org/dash128/Twitter_BackEnd/src/app/models"
	"bitbucket.org/dash128/Twitter_BackEnd/src/config/bd"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

/*UsuarioRepository -> manejador para los usuarios*/
type UsuarioRepository struct {
	// usuario models.Usuario
}

/*InsertarRegistro Metodo que inserta un usuairo*/
func (repository *UsuarioRepository) InsertarRegistro(usuario models.Usuario) (string, bool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := bd.MongoConexion.Database("db_twitter")
	col := db.Collection("usuarios")

	usuario.Password, _ = EncriptarPassword(usuario.Password)

	result, err := col.InsertOne(ctx, usuario)
	if err != nil {
		return "", false, err
	}

	ObjID, _ := result.InsertedID.(primitive.ObjectID)
	return ObjID.String(), true, nil
}

/*Listar -> función que lista usuarios*/
func (repository *UsuarioRepository) Listar() []*models.Usuario {
	data := make([]*models.Usuario, 0, 10)
	//user := models.Usuario{Nombre: "dash"}
	user := new(models.Usuario)
	user.Nombre = "Jordy"
	data = append(data, user)
	return data
}

/*ListarPorCorreo -> */
func (repository *UsuarioRepository) ListarPorCorreo() {

}

/*ExisteUsuario : email*/
func (repository *UsuarioRepository) ExisteUsuario(email string) (models.Usuario, bool, string) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := bd.MongoConexion.Database("db_twitter")
	col := db.Collection("usuario")

	condicion := bson.M{"email": email}

	var resultado models.Usuario

	err := col.FindOne(ctx, condicion).Decode(&resultado)
	ID := resultado.ID.Hex()
	if err != nil {
		return resultado, false, ID
	}

	return resultado, true, ID
}

/*EncriptarPassword -> Metodo que encripta el password del usuario*/
func EncriptarPassword(pass string) (string, error) {
	costo := 8
	bytes, err := bcrypt.GenerateFromPassword([]byte(pass), costo)

	return string(bytes), err
}
