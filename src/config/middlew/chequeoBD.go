package middlew

import (
	"net/http"

	"bitbucket.org/dash128/Twitter_BackEnd/src/config/bd"
)

/*ChequeoBD es el middlew que me permite conocer el estado de la BD*/
func ChequeoBD(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if bd.RevisarConexion() == false {
			http.Error(w, "Conexión perdida con la bd", 500)
			return
		}

		next.ServeHTTP(w, r)
	}
}
