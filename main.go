package main

import (
	"log"

	"bitbucket.org/dash128/Twitter_BackEnd/src/app/handlers"
	"bitbucket.org/dash128/Twitter_BackEnd/src/config/bd"
)

func main() {
	if bd.RevisarConexion() == false {
		log.Fatal("Sin conexión a la BD")
		return
	}

	handlers.Manejadores()
}
